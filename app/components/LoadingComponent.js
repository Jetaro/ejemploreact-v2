import React from "react";
import { StyleSheet, Text, View, ActivityIndicator } from "react-native";
import { Overlay } from "react-native-elements/dist/overlay/Overlay";

const styles = StyleSheet.create({
    overlay: {
      height: 100,
      width: 200,
      backgroundColor: "#fff",
      borderColor: "#00a680",
      borderWidth: 2,
      borderRadius: 10,
    },
    view: {
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
    },
    text: {
      color: "#00a680",
      textTransform: "uppercase",
      marginTop: 10,
    },
  });

export default function LoadingComponent(props) {
    const { visible, text } = props;

    return (
        <Overlay
          isVisible={visible?true:false}
          windowBackgroundColor="rgba(0, 0, 0, 0.5"
          overlayBackgroundColor="transparent"
          overlayStyle={styles.overlay}
        >
          <View style={styles.view}>
            <ActivityIndicator size="large" color="#00a680" />
            {text?'pum':'pam' && <Text style={styles.text}>{text?'pum':'pam'}</Text>}
          </View>
        </Overlay>
      );
}