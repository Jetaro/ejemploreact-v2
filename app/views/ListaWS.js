import * as React from 'react';
import { Component } from 'react';
import { View, Text, FlatList, Alert, Image } from 'react-native';
import  LoadingComponent  from '../components/LoadingComponent.js';

class ListaWS extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      initialPosition: 'unknown',
      res:[],
      out:[],
      load: false
    };
   }
      

   renderSeparator = () => {  
    return (  
        <View  
            style={{  
                height: 1,  
                width: "100%",  
                backgroundColor: "#000",  
            }}  
        />  
      );  
  };  

loadCom(a){ 
  return(<LoadingComponent visible={a}/>);
}

  //handling onPress action  
getListViewItem = (item) => {  

    this.setState({
      load:true
    });

    setTimeout(() => {
      console.log("2 sec.")
    }, 2000);

    fetch(item.url)
          .then(response => response.json())
          .then(result => {this.setState({ out : result } );
        
          this.setState({
            load:false
          });

          if (this.state.out && this.state.out!=[]){
            Alert.alert(
               `Detalles del item ${item.name}`,
               `
               Id Pokedex: ${JSON.stringify(this.state.out.id)}\n
               Peso: ${JSON.stringify(this.state.out.weight)} kgs \n
               Tipo predominante: ${JSON.stringify(this.state.out.types[0].type.name)} \n`,
               [
               //  {text: 'Preguntame despues', onPress: () => console.log('Ask me later pressed')},
               //  {text: 'Cancelar', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                 {text: 'Entendido', onPress: () => console.log(`Se abrio el item ${this.state.out.name}`)},
               ]
             ) } 
             else {
              Alert.alert(
                `Advertencia`,
                `Error de conexión: Vuelva e intente nuevamente.\nAsegurarse de tener conexión a Internet.`,
                [
                  {text: 'Entendido', onPress: () => console.log(`Error de conexión.`)},
                ]
              )
             }

        });
    console.log(()=> this.state.out.id);
  }  

  componentDidMount() {

      let text= this.props.route.params?
                      (this.props.route.params.text != null? this.props.route.params.text:""):"";

      let url = `https://pokeapi.co/api/v2/pokemon?limit=151`;//${this.props.id}
      if (text!="")
      url=`https://pokeapi.co/api/v2/pokemon?limit=${text}`;
  
      fetch(url)
          .then(response => response.json())
          .then(result => {this.setState({ res : result.results } );console.log(result.count);});
      
      if(!this.state.res){
        this.setState({ res : [
          {name: 'Devin'},
          {name: 'Dan'},
        ] })
      }

      console.log(this.state.initialPosition)
  }
  
  componentWillUnmount() {
      console.log(this.state.initialPosition)
  }

  App() {//<Image require={this.state.out?.sprites.front_default}/>
  return (
    <View style={{flex: 1, paddingTop:70, height: "100%", width: "100%",padding:20,backgroundColor:"#0e474f" }}>
    <Text style={{color:'#fff',fontSize:30}}>Lista</Text>
    
    <View style={{visible:this.state.load}}>
    {this.loadCom(this.state.load)}
    </View>

    <FlatList
        data={this.state.res}
        renderItem={({item}) => 
        <Text style={{color:'#fff',fontSize:20}} 
        onPress={this.getListViewItem.bind(this, item)}>
          {item.name}</Text>}  
        ItemSeparatorComponent={this.renderSeparator}  
      />

  </View>
  );
}

    render(){
      return (this.App())
    };

}

export default ListaWS;