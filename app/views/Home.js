import * as React from 'react';
import {Component,useState} from 'react';
import { View, Text, Button, Alert } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import { openDatabase } from 'react-native-sqlite-storage';
//import DatePicker from 'react-native-date-picker';

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      textPa:"",
      estado: 'No estas registrado',
      fecha: new Date(), 
    };
  }

  setFecha(date){
    this.setState({
      fecha: date,
    })
  }

  handleSubmit(e) {
    //e.preventDefault();
    console.log('You clicked submit.');
    this.props.navigation.navigate('Probando');
  }

  registro(){
    
    let db = openDatabase({ name: 'UserDatabase.db' });
    let usu = this.props.route.params.text1;
    let tala= 'No estas registrado';

    db.transaction(function (tx) {
      tx.executeSql(
        "SELECT * FROM table_user WHERE user_name = ?",
        [usu],
        (tx, results) => {
          console.log('Results', results.rows.length);
          if (results.rows.length > 0) {
            Alert.alert(
              'Mensaje',
              'Este usuario esta Registrado!!',
              [{
                  text: 'Ok',
                  onPress: () => {() => this.props.navigation.navigate('Bienvenido');},
                },],
              { cancelable: false }
            );} else alert('Fallido!');
        }, 
        (tx, error) => {
          console.log('Error:', error.code);}
      );
      });

      return tala;
  }

  componentDidMount() {

    let db = openDatabase({ name: 'UserDatabase.db' });

    db.transaction(function (txn) {
        txn.executeSql(
          "SELECT * FROM table_user where user_name = ?",
          [this.props.route.params.text1],
          function (tx, res) {
            console.log('home item:', res.rows.length);
            if (res.rows.length != 0) {
             this.setState({
               estado: 'Estas registrado'
             })
            } 
          },
            function (tx, error) {
              console.log('error:', error);
            }
        );
      });

  }

  handleSubmit2(e) {
    //e.preventDefault();
    console.log('You clicked submit 2.');
    if(this.state.textPa)
    this.props.navigation.navigate({name:'Lista',params:{text: this.state.textPa}});
    else
    this.props.navigation.navigate('Lista');
  }

  render(){
    return (
      <View 
      style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: '#0e474f'}}>
        <View style={{ flex: 1}}></View>
        <View style={{ flex: 3, padding:20}}>
        <Text style={{ fontSize:30, color:"#fff", textAlign:'left' }} textAlign='left'>
          Bienvenido a Genesys Ltda. </Text>
        <Text style={{ fontSize:30, color:"#fff" }}>{this.props.route.params?
                      (this.props.route.params.text1 != null? this.props.route.params.text1:""):""}</Text>
        </View>
        <View style={{ flex: 1}}>
        <Text style={{ fontSize:24, color:"#fff" }}>{}</Text>
        <Button
              style ={{paddingHorizontal:8,
                paddingVertical: 6,
                borderRadius: 4,
                backgroundColor: "oldlace",
                alignSelf: "flex-start",
                marginHorizontal: "1%",
                marginBottom: 50,
                minWidth: "48%",
                textAlign: "center"}}
              title="Verificar"
              onPress={()=>this.registro()} />
        </View>
        <View style={{ flex: 0.5}}><Button
              style ={{paddingHorizontal:8,
                paddingVertical: 6,
                borderRadius: 4,
                backgroundColor: "oldlace",
                alignSelf: "flex-start",
                marginHorizontal: "1%",
                marginBottom: 50,
                minWidth: "48%",
                textAlign: "center"}}
              title="Ingreso de actividades"
              onPress={()=>this.handleSubmit()} />{/*
              <DatePicker 
              fadeToColor={'orange'}
              mode={'date'}
              date={this.state.fecha} 
              onDateChange={(d)=>{this.setFecha(d)}} />*/}
        </View>
        <View style={{ flex: 0.1}}></View>
        <View style={{ flex: 0.5, flexDirection:'row'}}>
        <TextInput style={{borderColor: 'gray',borderWidth: 1}} textContentType='creditCardNumber'
              onChangeText={(v)=>this.setState({textPa:v})} 
              placeholder='Numero' placeholderTextColor={'gray'}></TextInput>
          <Button
              style ={{paddingHorizontal:8,
                paddingVertical: 6,
                borderRadius: 4,
                backgroundColor: "oldlace",
                alignSelf: "flex-start",
                marginHorizontal: "1%",
                minWidth: "48%",
                textAlign: "center"}}
              title="Listar Primeros Pokemon"
              onPress={()=>this.handleSubmit2()} />
        </View>
        <View style={{ flex: 2}}><Text style={{ color:"#fff", textAlign:'left' }}>Por favor, vea una de las opciones en inicio.</Text></View>
      </View>
    );
  }//this.state.fecha
}