import * as React from 'react';
import { Component } from 'react';
import { View, Text , Alert , Button } from 'react-native';
import MapboxGL from "@react-native-mapbox-gl/maps";
import Geolocation from '@react-native-community/geolocation';
import { openDatabase } from 'react-native-sqlite-storage';

MapboxGL.setAccessToken("sk.eyJ1IjoiamV0YXJvIiwiYSI6ImNsMHRqNjJtbzA0cGozZ29hOXhxbmJtcTAifQ.NpRMO9Rqi7k_JLT-uqHCIg");

class Mapas extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      initialPosition: 'unknown',
      lastPosition: 'unknown',
      ini_x: 0,
      ini_y: 0,
      puntos: null,
    };

    Geolocation.getCurrentPosition(info => {
      //console.log(info)
      this.state = {
        geolocalizacion: info
      };
    })
   }
    
    //watchID: ?number = null;
  
    async componentDidMount() {
      Geolocation.getCurrentPosition(
        position => {
          const initialPosition = JSON.stringify(position);
          const ini_x = position.coords.latitude;
          const ini_y = position.coords.longitude;
          this.setState({initialPosition,ini_x,ini_y});
        },
        error => Alert.alert('Error', JSON.stringify(error)),
        {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000},
      );
      this.watchID = Geolocation.watchPosition(position => {
        const lastPosition = JSON.stringify(position);
        this.setState({lastPosition});
      });
      //console.log(this.state.initialPosition)    

      let points = await this.ExecuteQuery("SELECT * FROM table_position",[]);
      var pim = points.rows.raw();
      //console.log(pim)

      const rander = () => { return(
        pim.map(element => {  return(
        <MapboxGL.PointAnnotation
          key={`${element.pos_id}`}
          id={`${element.pos_id}`}
          coordinate={[parseFloat(element.pos_lat), parseFloat(element.pos_lon)]}>
          <View style={{
                    height: 20, 
                    width: 20, 
                    backgroundColor: '#ffcccc', 
                    borderRadius: 10, 
                    borderColor: '#000', 
                    borderWidth: 5
                  }} />
        </MapboxGL.PointAnnotation>)})
      )}

        this.setState({
          puntos: rander()
      })

      //console.log("rander:",this.state.puntos) 
    }
  
    componentWillUnmount() {
      //console.log(this.state.initialPosition)
      //this.watchID != null && Geolocation.clearWatch(this.watchID);
    }

    renderAnnotations = () => {
      return (   
        <>{<MapboxGL.PointAnnotation
          key="pointAnnotation"
          id="pointAnnotation"
          coordinate={[this.state.ini_y, this.state.ini_x]}>
          <View style={{
                    height: 30, 
                    width: 30, 
                    backgroundColor: '#00cccc', 
                    borderRadius: 50, 
                    borderColor: '#fff', 
                    borderWidth: 3
                  }} />
        </MapboxGL.PointAnnotation>}
        {this.state.puntos}
        </> 
      );
    }
    //{this.cargarPuntos()}

    camPress(feature){
        console.log('Coords:', feature.geometry.coordinates);

        let db = openDatabase({ name: 'UserDatabase.db' });
    let vamos=false;
    let pam =feature.geometry.coordinates;
    let pum =feature.geometry.coordinates[0];
    let pom =feature.geometry.coordinates[1];

        db.transaction(function (tx) {
          tx.executeSql(
            'INSERT INTO table_position (pos_name, pos_lat, pos_lon) VALUES (?,?,?)',
            [pam,pum,pom],
            (tx, results) => {
              console.log('Results', results.rowsAffected);
              if (results.rowsAffected > 0) {
                Alert.alert(
                  'Mensaje',
                  `Pos Registrado!!`,
                  [{
                      text: 'Ok'
                    },],
                  { cancelable: false }
                );} else alert('Fallido!');
            }, 
            (tx, error) => {
              console.log('Error:', error.code);}
          );
          },null,this.update);
      
}

        verPress(){ 
            let db = openDatabase({ name: 'UserDatabase.db' });
            db.transaction(function (tx) {
                tx.executeSql(
                  "SELECT * FROM table_position",[],
                  (tx, results) => {
                    console.log('Pos', results.rows.length);
                    if (results.rows.length > 0) {
                      Alert.alert(
                        'Mensaje',
                        `Resultados:\n ${results.rows.raw()}`,
                        [{
                            text: 'Ok',
                          },],
                        { cancelable: false }
                      );} else alert('Fallido!');
                  }, 
                  (tx, error) => {
                    console.log('Error:', error);}
                );
             });
    }

    ExecuteQuery = (sql, params = []) => new Promise((resolve, reject) => {
      let db = openDatabase({ name: 'UserDatabase.db' });
      db.transaction((trans) => {
        trans.executeSql(sql, params, (trans, results) => {
          resolve(results);
        },
          (error) => {
            reject(error);
          });
      });
    });

    async cargarPuntos(){ 
        let db = openDatabase({ name: 'UserDatabase.db' });
        let res = null;

        await db.transaction(function (tx) {
           tx.executeSql(
              "SELECT * FROM table_position",[],
              (tx, results) => {
                console.log('Pos', results.rows.length);
                if (results.rows.length > 0) {
                  res=results.rows.raw();
                  const rest= results.rows.raw();
                 //console.log('ele',element);     
                 } else alert('Fallido!');
              }, 
              (tx, error) => {
                console.log('Error:', error);} 
            );
         }); 
          
         return ( async() =>{ 
          <>{ await rest.map(element => {  return(
              <MapboxGL.PointAnnotation
                key={`${element.pos_id}`}
                id={`${element.pos_id}`}
                coordinate={[parseFloat(element.pos_lat), parseFloat(element.pos_lon)]}>
                <View style={{
                          height: 100, 
                          width: 100, 
                          backgroundColor: '#ffcccc', 
                          borderRadius: 10, 
                          borderColor: '#000', 
                          borderWidth: 3
                        }} />
              </MapboxGL.PointAnnotation>)
            }) }</>}
        );


}

  App() {
  return (
    <View style={{flex: 1, height: "100%", width: "100%" }}>
        <Button onPress={()=>this.verPress()} style={{}} title="Puntos Guardados"></Button>
    <Text>Lon:{this.state.ini_y},Lat:{this.state.ini_x}</Text>
    <MapboxGL.MapView
      styleURL={MapboxGL.StyleURL.Street}
      ref={c => (this._map = c)}
      onPress={(feature)=>this.camPress(feature)}
      zoomLevel={16}
      centerCoordinate={[this.state.ini_y, this.state.ini_x]}
      style={{flex: 1}}>
       <MapboxGL.Camera
          zoomLevel={16}
          centerCoordinate={[this.state.ini_y, this.state.ini_x]}
          animationMode={'flyTo'}
          animationDuration={0}
        >
      </MapboxGL.Camera>
      {this.renderAnnotations()}
    </MapboxGL.MapView>
  </View>
  );
}

    render(){
      return (this.App())
    };

}

export default Mapas;