import React,{Component} from 'react'
import { StyleSheet, View, Text, Button, Alert } from 'react-native'
import { TextInput } from 'react-native-gesture-handler';
import { openDatabase } from 'react-native-sqlite-storage';
import BcryptReactNative from 'bcrypt-react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  box: {
    width: 60,
    height: 60,
    marginVertical: 20,
  },
})

class Registro extends Component{
  constructor(props){
    super(props)
  
    this.state = {
      userName:'',
      userAddress:'Nada',
      userContact:'Nada',
      userPass:'',
      vamos:false
    };
  
  }

  async hasheame(v){ 
    let hash = v;
    try {
      let salt = await BcryptReactNative.getSalt(12);
      hash = await BcryptReactNative.hash(salt, v);
      console.log({ hash });
    } catch (e) {
      console.log({ e });
      return v;
    }
    return hash;
  }

  registrar(){
    let db = openDatabase({ name: 'UserDatabase.db' });
    let vamos=false;
    let pam =this.state.userName;
    let pum = this.state.userContact;
    let pom = this.state.userPass;
    let hash = this.hasheame(this.state.userPass).then(v=>{pom=v}).then(
      setTimeout(() => {
        console.log("1 sec.")
        console.log('pom:', pom)

        db.transaction(function (tx) {
          tx.executeSql(
            'INSERT INTO table_user (user_name, user_contact, user_address) VALUES (?,?,?)',
            [pam,'a','a'],
            (tx, results) => {
              console.log('Results', results.rowsAffected);
              if (results.rowsAffected > 0) {
                Alert.alert(
                  'Mensaje',
                  `Registrado!! \nPass encriptada: ${pom}`,
                  [{
                      text: 'Ok',
                      onPress: () => { vamos=true; () => this.props.navigation.navigate('Bienvenido');},
                    },],
                  { cancelable: false }
                );} else alert('Fallido!');
            }, 
            (tx, error) => {
              console.log('Error:', error.code);}
          );
          },null,this.update);

      }, 1000));
      
    };
  
    setPost1(v){
      this.setState({
        userName:v
      })
    }

    setPost2(v){

      this.setState({
        userPass:v
      })
    }

    escanear(){
      this.props.navigation.navigate("Cargador");
    }

  render(){  
    return (
      <View style={styles.container}>
        
        <TextInput 
          style ={styles.textInput}
          Name="campo1"
          value={this.state.userName}
          onChangeText={(v)=>this.setPost1(v)}
          placeholder="Ingresar nombre-clave"
          placeholderTextColor={"gray"}/>

          <TextInput 
          style ={styles.textInput}
          Name="campo2"
          value={this.state.userPass}
          onChangeText={(v)=>this.setPost2(v)}
          placeholder="Ingresar password"
          secureTextEntry={true}
          placeholderTextColor={"gray"}/>

        <Button
          title="Registrar usuario"
          onPress={() => {
          this.registrar()  
          }}
        />

        <Button
          title="Escanear QR"
          onPress={() => {
          this.escanear()  
          }}
        />

      </View>);
  }
}

export default Registro;