import React, {Component} from 'react';
import {useEffect} from 'react';
import { Text, View, Image, Button, StyleSheet, StatusBar, Modal } from 'react-native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { TextInput } from 'react-native-gesture-handler';
import { openDatabase } from 'react-native-sqlite-storage';

const Stack = createNativeStackNavigator();

/*function _onPressFun(){
  //this.setModalVisibility(true);
  return (alert("Registro de informacion para guerreros."));
}*/

const styles = StyleSheet.create({
    principal:{

    },
    container: {
      flex: 1,
      marginTop: StatusBar.currentHeight || 0,
    },
    textform:{
      color: '#fff'
    },
    textInput:{
      borderColor: 'gray',
      borderWidth: 2
    },
    btn:{
      paddingHorizontal:8,
      paddingVertical: 6,
      borderRadius: 4,
      backgroundColor: "oldlace",
      alignSelf: "flex-start",
      marginHorizontal: "1%",
      marginBottom: 50,
      minWidth: "48%",
      textAlign: "center"
    },
    item: {
      backgroundColor: '#f9c2ff',
      padding: 20,
      marginVertical: 8,
      marginHorizontal: 16,
    },
    title: {
      fontSize: 32,
    },
  });

  
const CatApp = () => {
  return (
    <View style={{ flex: 1, alignItems: 'center',alignContent: 'space-around' , justifyContent: 'space-evenly' }}>
    </View>
  );
}


//export default CatApp;
export default class LoginHome extends Component {
  constructor(props) {
    super(props);

    //this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    //let db = openDatabase({ name: 'UserDatabase.db' });

    this.state = {
      postext:"",
      postext2:"",
      estado: 'oli',
      visibility: false,
    };

    //this.Hint = React.createRef();
  }
  
  componentDidMount() {

    let db = openDatabase({ name: 'UserDatabase.db' });

      db.transaction(function (txn) {
        txn.executeSql(
          "SELECT name FROM sqlite_master WHERE type='table' AND name='table_user'",
          [],
          function (tx, res) {
            console.log('item:', res.rows.length);
            if (res.rows.length == 0) {
              txn.executeSql('DROP TABLE IF EXISTS table_user', []);
              txn.executeSql(
                'CREATE TABLE IF NOT EXISTS table_user(user_id INTEGER PRIMARY KEY AUTOINCREMENT, user_name VARCHAR(20), user_contact INT(10), user_address VARCHAR(255))',
                []
              );
            }
          },
            function (tx, error) {
              console.log('error:', error);
              if (res.rows.length == 0) {
                txn.executeSql('DROP TABLE IF EXISTS table_user', []);
                txn.executeSql(
                  'CREATE TABLE IF NOT EXISTS table_user(user_id INTEGER PRIMARY KEY AUTOINCREMENT, user_name VARCHAR(20), user_contact INT(10), user_address VARCHAR(255))',
                  []
                );
             }
            },
            txn.executeSql(
              "SELECT name FROM sqlite_master WHERE type='table' AND name='table_position'",
              [],
              function (tx, res) {
                console.log('posi:', res.rows.length);
                if (res.rows.length == 0) {
                  txn.executeSql('DROP TABLE IF EXISTS table_position', []);
                  txn.executeSql(
                    'CREATE TABLE IF NOT EXISTS table_position(pos_id INTEGER PRIMARY KEY AUTOINCREMENT, pos_name VARCHAR(20), pos_lat VARCHAR(255), pos_lon VARCHAR(255))',
                    []
                  );
                }
              },
                function (tx, error) {
                  console.log('error2:', error);
                  if (res.rows.length == 0) {
                 }
           })
        );
      });
      /**/
 }

  setModalVisibility(visible) {
    this.setState({
      visibility: visible,
    });
  }

  setPost1(value){
    this.setState({
      postext: value,
    })
  }

  setPost2(value){
    this.setState({
      postext2: value,
    })
  }

  _onPressFun(){
    this.setModalVisibility(true);
    alert("Registro de informacion para guerreros.");
  }

  postMsg(){
      const t1 = this.state.postext;
      const t2 = this.state.postext2;
      console.log(t1+t2);
      this.props.navigation.navigate({name:'Bienvenido',params:{text1: t1, text2: t2}});
  }

  render(){
    return (
      <View navigation={this.props.navigation} 
      style={{ flex:1, backgroundColor: '#0e474f', alignItems: 'center',alignContent: 'space-around' , justifyContent: 'space-between' }}>
        
        <View style={{ flex:1 }}></View>

        <View style={{ padding:1, flex:1 }}>
        <Image
          source={require('../assets/img/logo.png')}/*{uri: "https://reactnative.dev/docs/assets/p_cat1.png"}*/
          style={{  backgroundColor: 'skyblue'}}
        /></View>

        <View style={{ flex:1 }}></View>
        
        <View style={{ flex:1, width:'80%' }}>
        <Text style ={styles.textform}>Nombre-clave</Text>
        <TextInput 
          style ={styles.textInput}
          Name="campo1"
          value={this.state.postext}
          onChangeText={(v)=>this.setPost1(v)}
          placeholder="Ingresar nombre-clave"
          placeholderTextColor={"gray"}/>
        <Text style ={styles.textform}>Contraseña</Text>
        <TextInput 
          style ={styles.textInput}
          value={this.state.postext2}
          onChangeText={(v)=>this.setPost2(v)}
          Name="campo2" 
          placeholder="Ingresar contraseña"
          placeholderTextColor={"gray"}/>  
        </View>
        
        <View style={{ flex:1 }}></View>
        
        <View style={{ flex:1, flexDirection: 'row',alignContent: 'space-around', justifyContent: 'space-evenly'}}>
        <View ><Button
            style ={styles.btn}
            title="Enviar Formulario"
            onPress={() => this.postMsg()} /></View>
        <View style={{ minWidth:10}}></View>
        <View ><Button
            style ={styles.btn}
            title="Información"
            onPress={()=>this._onPressFun()}/></View>
        </View>

        <View style={{ flex:1 }}>
        <Modal
          style={{ flex: 1, alignItems: 'center',alignContent: 'space-around' , justifyContent: 'space-evenly' }}
          animationType={'slide'}
          transparent={false}
          visible={this.state.visibility}>
          <View style={styles.modalContainer}>
            <View>
              <Text>Esta es la informacion de registro:</Text>
              <Text>-Mayor de 18 años.</Text>
              <Text>-No pertenecer a otros clanes.</Text>
              <Text>-En lo posible, ser un humano o gato!</Text>
              <Button
                color="#000"
                onPress={() => this.setModalVisibility(!this.state.visibility)}
                title="Cerrar información"
              />
            </View>
          </View>
        </Modal>
        </View>

      </View>);
  }
}
